<?php

namespace Mamont\Utils;

class DataObject
{
    protected $data = [];

    public function __construct(array $data = [])
    {
        $this->data = array_merge($this->data,  $data);
    }

    public function __call($method, $args)
    {
        $errorNotDefined = "Method '$method' is not defined";
        $errorWrongArgCount = "Method '$method' expects exactly 1 argument, " . count($args) . " provided";

        $matches = [];
        if (!preg_match('/^(get|set)(.*)$/', $method, $matches)) {
            throw new \RuntimeException($errorNotDefined);
        }

        $method = $matches[1];
        $field = strtolower($matches[2]);

        if (!array_key_exists($field, $this->data)) {
            throw new \RuntimeException($errorNotDefined);
        }

        if ($method === "get") {
            return $this->data[$field];
        }

        if (count($args) !== 1) {
            throw new \InvalidArgumentException($errorWrongArgCount);
        }

        $this->data[$field] = $args[0];

        return $this;
    }

    public function toArray()
    {
        return $this->data;
    }

    public function toString()
    {
        return json_encode($this->toArray(), JSON_PRETTY_PRINT);
    }
}
