<?php

namespace Mamont\Utils;

class DataObjectImmutable extends DataObject
{
    public function __call($method, $args)
    {
        if (!preg_match('/^get(.*)$/', $method)) {
            throw new \RuntimeException("Method '$method' is not defined");
        }

        return parent::__call($method, $args);
    }
}
