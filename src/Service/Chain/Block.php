<?php

namespace Mamont\Service\Chain;

use Mamont\Utils\DataObjectImmutable;
use Mamont\Service\Chain;

/**
 * Single block implementation
 */
class Block extends DataObjectImmutable
{
    /**
     * @var array $data Block data defaults
     */
    protected $data = [
        'nextblockhash' => null,
        'previousblockhash' => null
    ];

    /**
     * @var Mamont\Service\Chain $api Blockchain API instance
     */
    protected $api = null;

    /**
     * Class constructor
     *
     * @param Mamont\Service\Chain $api Blockchain API instance
     * @param mixed $id Block hash or height
     *
     */
    public function __construct(Chain $api, $id)
    {
        $this->api = $api;

        // convert block height to block hash
        if (is_int($id)) {
            $id = $this->api->getblockhash($id);
        }

        // FIXME: avoid recusion
        parent::__construct($this->api->getblock($id));
    }

    /**
     * Get transactions
     *
     * @return array Mamont\Service\Chain\Transaction block transactions
     */
    public function getTx()
    {
        // its a bug/feature that you can't get info about transactions
        // in the genesis block, so just return empty array, nothing 
        // useful there anyway
        if ($this->getHeight() === 0) {
            return [];
        }

        foreach ($this->data['tx'] as $tx) {
            yield new Transaction($this->api, $tx);
        }
    }

    /**
     * Get transactions alias
     *
     * @return array Mamont\Service\Chain\Transaction block transactions
     */
    public function getTransactions()
    {
        yield from $this->getTx();
    }

    /**
     * Get block time
     *
     * @return DateTime block time
     */
    public function getTime()
    {
        return new \DateTime("@{$this->data['time']}");
    }
}
