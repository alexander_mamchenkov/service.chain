<?php

namespace Mamont\Service\Chain;

use Mamont\Utils\DataObjectImmutable;
use Mamont\Service\Chain;
use Mamont\Service\Chain\Block;
use Mamont\Service\Chain\Transaction\Vin;
use Mamont\Service\Chain\Transaction\Vout;

class Transaction extends DataObjectImmutable
{
    /**
     * @var Mamont\Service\Chain Blockchain API instance
     */
    protected $api = null;

    /**
     * Class constructor
     *
     * @param Mamont\Service\Chain $api Blockchain API instance
     * @param string $hash Transaction hash
     *
     */
    public function __construct(Chain $api, $hash)
    {
        $this->api = $api;

        parent::__construct($this->api->getrawtransaction($hash, 1));
    }

    /**
     * Get transaction inputs
     *
     * @return array Mamont\Service\Chain\Transaction\Vin Transaction inputs
     */
    public function getInputs()
    {
        foreach ($this->data['vin'] as $in) {
            if (array_key_exists('txid', $in)) {

                // for transfer type of inputs
                yield new Vin($this->api, [
                    'tx'        => $this->getTxid(),
                    'vouttx'    => $in['txid'],
                    'voutidx'   => $in['vout'],
                    'type'      => Vin::T_TRANSFER
                ]);
            } else {

                // for coinbase type of inputs
                yield new Vin($this->api, [
                    'tx'        => $this->getTxid(),
                    'type'      => Vin::T_COINBASE
                ]);
            }
        }
    }

    /**
     * Get transaction outputs
     *
     * @return array Mamont\Service\Chain\Transaction\Vout Transaction outputs
     */
    public function getOutputs()
    {
        foreach ($this->data['vout'] as $out) {
            yield new Vout($this->api, [
                'value'     => $out['value'],
                'tx'        => $this->getTxid(),
                'idx'       => $out['n'],
                'addresses' => $out['scriptPubKey']['addresses'],
            ]);
        }
    }

    /**
     * Get single output
     *
     * @param int $idx Output index
     *
     * @return Mamont\Service\Chain\TransactionVout Transaction output
     */
    public function getOutput($idx)
    {
        foreach ($this->getOutputs() as $out) {
            if ($out->getIdx() == $idx) {
                return $out;
            }
        }
    }

    /**
     * Get transaction block
     *
     * @return Mamont\Service\Chain\Block
     */
    public function getBlock()
    {
        return new Block($this->api, $this->data['blockhash']);
    }

    /**
     * Get transaction time
     *
     * @return DateTime transaction time
     */
    public function getTime()
    {
        return new \DateTime("@{$this->data['time']}");
    }

    /**
     * Get total transaction value
     *
     * @return int total transaction value
     */
    public function getTotalValue()
    {
        $value = 0;
        foreach ($this->getOutputs() as $out) {
            $value += $out->getValue();
        }

        return $value;
    }
}
