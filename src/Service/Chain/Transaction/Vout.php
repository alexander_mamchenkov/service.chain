<?php

namespace Mamont\Service\Chain\Transaction;

use Mamont\Utils\DataObjectImmutable;
use Mamont\Service\Chain;
use Mamont\Service\Chain\Transaction;

/**
 * Transaction output
 */
class Vout extends DataObjectImmutable
{
    /**
     * @var Mamont\Service\Chain Blockchain API instance
     */
    protected $api = null;

    /**
     * Class constructor
     *
     * @param Mamont\Service\Chain $api Blockchain API instance
     * @param array $data Output data
     */
    public function __construct(Chain $api, $data)
    {
        $this->api = $api;

        parent::__construct($data);
    }

    /**
     * Get output transaction
     *
     * @return Mamont\Service\Chain\Transaction output transaction
     */
    public function getTx()
    {
        return new Transaction($this->api, $this->data['tx']);
    }

    /**
     * Get output transaction alias
     *
     * @return Mamont\Service\Chain\Transaction output transaction
     */
    public function getTransaction()
    {
        return $this->getTx();
    }

    /**
     * Check if transaction output is spent
     *
     * @return bool true if spent or false if not
     */
    public function isSpent()
    {
        return ($this->api->gettxout($this->data['tx'], $this->data['idx']) === null) ? true : false;
    }
}
