<?php

namespace Mamont\Service\Chain\Transaction;

use Mamont\Utils\DataObjectImmutable;
use Mamont\Service\Chain;
use Mamont\Service\Chain\Transaction;

/**
 * Transaction input
 */
class Vin extends DataObjectImmutable
{
    // input types constants
    const T_COINBASE = "coinbase";
    const T_TRANSFER = "transfer";

    /**
     * @var Mamont\Service\Chain Blockchain API instance
     */
    protected $api = null;

    /**
     * Class constructor
     *
     * @param Mamont\Service\Chain $api Blockchain API instance
     * @param array $data Input data
     */
    public function __construct(Chain $api, $data)
    {
        $this->api = $api;

        parent::__construct($data);
    }

    /**
     * Get input transaction
     *
     * @return Mamont\Service\Chain\Transaction
     */
    public function getTx()
    {
        return new Transaction($this->api, $this->data['tx']);
    }

    /**
     * Get input transaction alias
     *
     * @return Mamont\Service\Chain\Transaction
     */
    public function getTransaction()
    {
        return $this->getTx();
    }

    /**
     * Get corresponding transaction output
     *
     * @return Mamont\Service\Chain\Transaction\Vout transaction output
     */
    public function getVout()
    {
        if ($this->getType() !== self::T_TRANSFER) {
            throw new \RuntimeException("Impossible to get Vout for non-transfer type of transactions");
        }
        return (new Transaction($this->api, $this->getVoutTx()))->getOutput($this->getVoutIdx());
    }

    /**
     * Get input value
     *
     * @return int input value
     */
    public function getValue()
    {
        if ($this->getType() !== self::T_TRANSFER) {
            return $this->getTx()->getTotalValue();
        }

        return $this->getVout()->getValue();
    }
}
