<?php

namespace Mamont\Service;

use Mamont\Service\Chain\Block;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

/**
 * Class for basic interaction with JSON-RPC blockchains
 */
class Chain
{
    /**
     * @var string $uri JSON-RPC URI
     */
    protected $config = [];

    /**
     * @var GuzzleHttp\Client $api HTTP Client instance
     */
    protected $api = null;

    /**
     * Class constructor
     * 
     * @param array $config Config parameters
     */
    public function __construct($config)
    {
        foreach (['host', 'port', 'user', 'pass'] as $field) {
            if (!isset($config[$field]) || empty($config[$field])) {
                throw new \RuntimeException("Missing required '$field' config parameter");
            }
        }
        $this->config = $config;

        $this->api = new Client(['base_uri' => $config['host'] . ':' .  $config['port']]);
    }

    /**
     * Call JSON-RPC method with given params
     *
     * @param string $method Method name to call
     * @param array $params Parameters to pass to method
     *
     * @return mixed Request result data
     */
    public function exec($method, $params = [])
    {
        if (!is_array($params)) {
            $params = [$params];
        }
        try {
            $res = $this->api->get('/',[
                'json' => [
                    'method' => $method,
                    'params' => $params,
                    'id' => time()
                ],
                'auth' => [
                    $this->config['user'],
                    $this->config['pass']
                ]
            ]);
        } catch (ClientException $e) {
            throw new \RuntimeException($e->getMessage(), $e->getCode());
        }

        $body = json_decode((string) $res->getBody(), true);

        return $body['result'];
    }

    /**
     * Get block by hash or height number
     *
     * @param $id Block hash
     */
    public function block($id)
    {
        return new Block($this, $id);
    }

    /**
     * Magic method to allow shortcut calls for JSON-RPC methods directly
     *
     * @param string $method Method name
     * @param array $params Method parameters to use
     *
     * @return mixed Request result data
     */
    public function __call($method, $params = [])
    {
        return call_user_func_array([$this, 'exec'], [$method, $params]);
    }

}
