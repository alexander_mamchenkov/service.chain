service.chain
=====================

This is a basic package to read from blockchain JSON-RPC API in a more or less conviniet way

Setup
-----

Setup private repo that is hosting the package and add package to composer

```
composer config repositories.mamont-service-chain vcs https://bitbucket.org/alexander_mamchenkov/service.chain.git
composer require mamont/service-chain:*
composer update
```

Usage
-----

```
<?php

require_once 'vendor/autoload.php';

$chain = new Mamont\Service\Chain([
    'host'  => '127.0.0.1',
    'port'  => '22823',
    'user'  => 'RPCUser',
    'pass'  => 'PRCPass'
]);

print $chain->block(1)->toString();

foreach ($chain->block(2)->getTransactions() as $tx) {
    print "Transaction [" . $tx->getTxid() . "] with total value of [" . $tx->getTotalValue() . "]\n";

    print $tx->toString();
}
```

Hits
-----

* All objects (Block, Transaction, Vin, Vout) have the following methods:
  * toString(): return pretty json of object data
  * toArray(): return array of internal object data
* You can call any blockchain API method directly on Chain instance, for example:
  * $chain->getinfo()
  * $chain->getBlockHash(11)
  * $chain->getrawtransaction(someTransactionHash)
* While data inside objects is kept plain for simplicity, any related data will be returned
  as an array of corresponding objects when called via access methods, for example:
  * $chain->block(111)->getTransactions() will return a list of transaction ojects
  * $chain->block(12)->getTransactions()[0]->getOutputs() will return a list of vout objects
* Consider this project as a wrapper around blockchain JSON-RPC only, no magic

Limitations
-----

* Due to coind implementations (at least bitcoin/litecoin) you can't really get info about transactions in genesis block
* There is nothing here about blockchain addresses, as calculating their balance, etc, is not as easy and is out of scope
  of this project
